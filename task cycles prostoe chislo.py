import math
count = 0
number = int(raw_input('Enter prime number(s): \n'))

while number > 1:
    f = True
    for i in range(2, int(round(math.sqrt(number)) + 1)):
        if number % i == 0:
            f = False
    if f:
        count += 1
    number = int(raw_input())

print 'The number of entered prime numbers is %i' % count