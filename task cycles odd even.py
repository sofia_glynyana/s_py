__author__ = 'sonya'
n = int(raw_input("""Enter the number. A large one: \n"""))

even = 0
odd = 0

while n > 0:
    if n % 2 == 0:
        even += 1
    else:
        odd += 1
    n //= 10

print 'Even number: %d, odd number: %d' % (even, odd)