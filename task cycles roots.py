#!/sonya/local/bin/python
# -*- coding: utf-8 -*-

# Найти все возможные корни уравнения вида
# ax + by + cz = d, где a, b, c, d - задаются,
# x, y, z - требуется найти.
# Все числа (коэффициенты и переменные) - натуральные числа (т.е. > 0).
d = int(raw_input('d = '))
a = int(raw_input('a = '))
b = int(raw_input('b = '))
c = int(raw_input('c = '))

for i in range (1, d//a):
    for j in range (1, d//b):
        for k in range (1, d//c):
            if a*i + b*j + c*k == d:
                print '%d*%d + %d*%d + %d*%d = %d' % (a, i, b, j, c, k, d)