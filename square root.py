__author__ = 'sonya'
a = float(raw_input('Please enter the meaning of a: \n'))
b = float(raw_input('Now enter the meaning of b: \n'))
c = float(raw_input('And finally, enter c: \n'))
import math
disc = float(b**2 - 4*a*c)  #discriminant
print "The discriminant value is ", disc

if disc > 0:
    x_1 = float((-b - (math.sqrt(disc)))/2*a)
    x_2 = float((-b + (math.sqrt(disc)))/2*a)
    print 'The root one is %.2f' % x_1
    print 'The root two is %.2f' % x_2

elif disc == 0:
    x = float(-b / 2*a)
    print 'The root of the equation is %.2f' % x
else:
    print 'Your equation can\'t be solved'
quit()
